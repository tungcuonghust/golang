package main

import (
	"io"
	"log"
	"net/http"
	"fmt"
	restful "github.com/emicklei/go-restful"
)

// This example shows the minimal code needed to get a restful.WebService working.
//
// GET http://localhost:8080/hello

func main() {
	ws := new(restful.WebService)
	ws.
		Path("/v1").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)
	ws.Route(ws.GET("/user/{name}").To(getWithName))
	ws.Route(ws.POST("/user/"))
	restful.Add(ws)
	fmt.Println("Server running on: http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func getWithName(req *restful.Request, resp *restful.Response) {
	io.WriteString(resp, fmt.Sprintf("Hello %s", req.PathParameter("name")))
}