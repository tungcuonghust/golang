package main

import "github.com/gin-gonic/gin"
import "net/http"

func main() {
	router := gin.Default()
	v1 := router.Group("/v1")
	{
		// This handler will match /user/john but will not match /user/ or /user
		v1.GET("/user/:name", func(c *gin.Context) {
			name := c.Param("name")
			c.String(http.StatusOK, "Hello %s", name)
		})

		// However, this one will match /user/john/ and also /user/john/send
		// If no other routers match /user/john, it will redirect to /user/john/
		v1.GET("/user/:name/*action", func(c *gin.Context) {
			name := c.Param("name")
			action := c.Param("action")
			message := name + " is " + action
			c.String(http.StatusOK, message)
		})

		// For each matched request Context will hold the route definition
		v1.POST("/user/:name/*action", func(c *gin.Context) {
			b := c.FullPath() == "/v1/user/:name/*action" // true
			c.String(http.StatusOK, "%t", b)
		})

		// This handler will add a new router for /user/groups.
		// Exact routes are resolved before param routes, regardless of the order they were defined.
		// Routes starting with /user/groups are never interpreted as /user/:name/... routes
		v1.GET("/user/groups", func(c *gin.Context) {
			c.String(http.StatusOK, "The available groups are [...]")
		})
	}
	router.Run(":8080")
}