package main

import (
	"fmt"
	"reflect"
)

func main() {

	strArray1 := []string{"Japan", "Australia", "Germany"}
	strArray2 := strArray1  // data is passed by value
	// deep copy
	var deepCopyArray []string
	// deepCopyArray := make([]string, len(strArray1))
	deepCopyArray = append(deepCopyArray, strArray1...)
	// copy(deepCopyArray, strArray1)
	fmt.Printf("deepCopyArray: %v\n", deepCopyArray)
    // shadow copy
	fmt.Printf("strArray1: %v\n", strArray1)
	fmt.Printf("strArray2: %v\n", strArray2)	
	strArray1[0] = "Canada"
	fmt.Printf("strArray1: %v\n", strArray1)
	fmt.Printf("strArray2: %v\n", strArray2)
	fmt.Printf("deepCopyArray: %v\n", deepCopyArray)	
	fmt.Println(reflect.ValueOf(strArray1).Kind())

}
