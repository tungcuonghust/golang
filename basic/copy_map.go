package main

import "fmt"
func deepCopyMap(originalMap map[string]int, targetMap map[string]int){
	for key, value := range originalMap {
		targetMap[key] = value
	  }
}
func main() {

	map1 := map[string]int{"India": 20, "Canada": 70, "Germany": 15}
	// deep copy
	map2 := make(map[string]int)
	deepCopyMap(map1, map2)
	// shadow copy 
	map3 := map1
	
	fmt.Printf("map1: %v\n", map1)
	fmt.Printf("map2: %v\n", map2)	
	map1["India"] = 21
  
	fmt.Printf("map1: %v\n", map1)
	fmt.Printf("map2: %v\n", map2)	
	fmt.Printf("map3: %v\n", map3)	
}
