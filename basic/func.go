package main

import "fmt"

// Function accepting arguments
func add(x *int) {
	*x = *x + 1
}

func main() {
	// Passing arguments
	x := 1
	add(&x)
	fmt.Println(x)
}