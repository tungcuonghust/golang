package main
 
import (
	"fmt"
	"reflect"
)
 
func main() {
	var employee = map[string]int{}
	fmt.Println(employee) // Initial Map
 
	employee["Rocky"] = 30 // Add element
	employee["Josef"] = 40
	delete(employee, "Josef")
	fmt.Println(employee)
	fmt.Println(reflect.ValueOf(employee).Kind())
}