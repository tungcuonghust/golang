package main

import (
	"fmt"
	"reflect"
)

func main() {
	var strSlice = []string{"India", "Canada", "Japan", "Germany", "Italy"}
	fmt.Println(strSlice)

	strSlice = RemoveIndex(strSlice, 3)
	fmt.Println(strSlice)
	fmt.Println(reflect.ValueOf(strSlice).Kind())
}

func RemoveIndex(s []string, index int) []string {
	return append(s[:index], s[index+1:]...)
}